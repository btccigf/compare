#!/usr/bin/perl
#
# (no handling of Office files, could be done if necessary)
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# collect and compare IP within text files or PDF
# (no handling of Office files, could be done if necessary)

use strict;

# convert PDF to text, mandatory
my $pdftotext = "/usr/bin/pdftotext";
die "$pdftotext missing. Exiting " unless -x $pdftotext;

# debug tool
use Data::Dumper qw(Dumper);

# tempdir
use File::Temp qw(tempfile);

# recursive search, filename parsing, etc
use File::Find;
use Digest::MD5::File qw/file_md5_base64/;
use File::Basename;

# string comparison
use String::Similarity;
use Text::Levenshtein qw(distance);

# find IPs
use Regexp::Common qw /net time/;   # libregexp-common-net-cidr-perl libregexp-common-time-perl
my $skip_private_IP = 1;


($#ARGV == 0) or die "Usage: $0 [directory]\n";

## run

# get list of tiles
my %files;
find(sub {$files{$File::Find::name} = file_md5_base64($_) if -f;}, @ARGV);
print scalar(keys(%files))." files to compare...\n";

# read each file and collect IPS
my %ips; 
foreach my $file (keys(%files)) {
    #print "$files{$file}, $file\n";

    # find out suffix
    my $suffix;
    if ($file =~ /^(.*)(\.[^\.]*)$/) { $suffix = lc($2); }
        
    # if PDF, convert to text into tempfile
    my $workfile = $file;
    if ($suffix eq ".pdf") {	
	my (undef, $workfile) = tempfile(UNLINK => 1);
	system($pdftotext, $file, $workfile);	
    }
    
    # text file
    open(FILE, "< $workfile") or print "Unable to read $workfile\n";   
   
    while (<FILE>) {
	# IPv4 collection
	while (/($RE{net}{IPv4})/g) {
	    # (should we skip private addresses?)
	    if ($skip_private_IP) {
		my ($a,$b,undef,undef) = split(/\./, $1);
		# 127.0.0.0 	127.255.255.255 
		next if $a eq 127;
		# 10.0.0.0 	10.255.255.255 
		next if $a eq 10;
		# 172.16.0.0 	172.31.255.255
		next if $a eq 172 and ($b > 15 and $b < 32);
		# 192.168.0.0 	192.168.255.255
		next if $a eq 192 and $b eq 168;
	    }
	    $ips{$1}{$file} = 1;
	}
	# IPv6 collection
	while (/($RE{net}{IPv6})/g) {
	    # (should we skip private addresses?)
	    if ($skip_private_IP) {
		next if $1 eq "::1";
	    };	   
	    $ips{$1}{$file} = 1;
	}
    }
    close(FILE);

}

#print Dumper \%ips;

# output data in ODS with unique name
# Three files    FULL contains every IPS
#                ... (EXPURGED) contains only IP appearing more than once
#                PERFOLDER contains only IP appearing more than once different final folder
#                PERPATH  contains only IP appearing with dir path quite different 

my $unique = 0;
my $csv_expurged = "IP-collect+compare.$unique.csv";
my $csv_full = "IP-collect+compare.$unique.FULL.csv";
my $csv_perfolder = "IP-collect+compare.$unique.PERFOLDER.csv";
my $csv_perpath = "IP-collect+compare.$unique.PERPATH.csv";
while (-e $csv_expurged or -e $csv_full or -e $csv_perfolder or -e $csv_perpath) {
    $unique++;
    $csv_expurged = "IP-collect+compare.$unique.csv";
    $csv_full = "IP-collect+compare.$unique.FULL.csv";
    $csv_perfolder = "IP-collect+compare.$unique.PERFOLDER.csv";
    $csv_perpath = "IP-collect+compare.$unique.PERPATH.csv";
    die "too many previous run output files" if $unique > 200;
}
open(EXPURGED, "> $csv_expurged");  
open(FULL, "> $csv_full");   
open(PERFOLDER, "> $csv_perfolder");  
open(PERPATH, "> $csv_perpath");    

# output will be like
#   IP, file
#     , file
#     , file
#   OTHER IP, file
#           , file
my $title = "IP,Chemin,Fichiers,MD5 base64";
print FULL "$title\n";
print EXPURGED "$title\n";
print PERFOLDER "$title\n";
print PERPATH "$title\n";

foreach my $ip (keys(%ips)) {
    my $files_per_ip = scalar(keys %{ $ips{$ip}});
    $files_per_ip = 0 if $files_per_ip < 2;

    print FULL "$ip";
    print EXPURGED "$ip" if $files_per_ip;

    my $kept_content;
    my ($more_than_one_dir, $very_different_path, $last_dir_was);
	
    foreach my $file (keys %{ $ips{$ip} }) {
	my $dirname = dirname($file);
	my $filename = basename($file);
	    
	my $content = ",$dirname,$filename,$files{$file}\n";

	print FULL $content;
	
	# the rest is useless for IP appearing only in one file
	last unless $files_per_ip;
	
	print EXPURGED $content;
	$kept_content .= $content;

	# mark if there is more than one dir
	$more_than_one_dir = 1 if $last_dir_was and $last_dir_was ne $dirname;

	# check if path are similar or not much
	# make sense only if last_dir_was is set
	#    similarity give result from to 0 to 1, 1 = identical
	#    levenshtein give results the number of edits required to go from one string to the other, so bigger the number, the more different it is
	if ($last_dir_was) {
	    my $compare_similarity = similarity($last_dir_was, $dirname);
	    my $compare_levenshtein = distance($last_dir_was, $dirname);
	    # would probably require some ajustement
	    if ($compare_similarity < 0.5 and $compare_levenshtein > (length($dirname) / 2)) {
		#print "DIFF! $compare_similarity $compare_levenshtein\n\t$last_dir_was\n\t$dirname\n";
		$very_different_path++;
	    } 
	}
	
	# update the last dir marker
	$last_dir_was = $dirname;
    }

    # finally print in case there is more than one folder
    # (require specific handling since it cannot be predicted)
    print PERFOLDER $ip.$kept_content if $more_than_one_dir;
    
    # finally print in case path is very different
    # (require specific handling since it cannot be predicted)    
    print PERPATH $ip.$kept_content if $very_different_path;

    
    
    
}
close(EXPURGED);
close(FULL);
close(PERFOLDER);
close(PERPATH);


# EOF

