#!/usr/bin/perl
#  (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# list and compare all files md5 sum

use strict;

# add local library
use lib "lib";

# debug tool
use Data::Dumper qw(Dumper);

# tempdir
use File::Temp qw(tempfile);

# recursive search, filename parsing, etc
use File::Find;
use Digest::MD5::File qw/file_md5_base64/;
use File::Basename;

# readable filesize
use Number::Bytes::Human qw/format_bytes/;



($#ARGV == 0) or die "Usage: $0 [directory]\n";

## run

# get list of tiles
print "Obtain files list...\n";
my %files;
find(sub {$files{$File::Find::name} = file_md5_base64($_) if -f;}, @ARGV);
print scalar(keys(%files))." files to compare...\n";


my %md5;
my %size;
foreach my $file (keys(%files)) {
    # find out suffix
    my $suffix = ".NULL";
    my $filename = basename($file);
    if ($filename =~ /^(.+)(\.[^\.]*)$/) { $suffix = lc($2); }
    
    # get hash    
    my $hash = $files{$file};

    # store suffix/hash/file
    $md5{$suffix}{$hash}{$file} = 1;

    # store size
    $size{$hash} = (stat $file)[7] unless $size{$hash};
}


# #print Dumper \%ips;

# output data in ODS with unique name
# Three files    FULL contains every IPS
#                ... (EXPURGED) contains only IP appearing more than once
#                PERFOLDER contains only IP appearing more than once different final folder
#                PERPATH  contains only IP appearing with dir path quite different 

my $unique = 0;
my $csv_prefix = "list+compare";
my $csv_full = "$csv_prefix.$unique.FULL.csv";
while (-e $csv_full) {
    $unique++;
    $csv_full = "$csv_prefix.$unique.FULL.csv";
    die "too many previous run output files" if $unique > 200;
}
print "Write CSV...\n";
open(FULL, "> $csv_full");  

my $title = "MD5 base64, Taille, Fichier, Chemin,";
print FULL "$title\n";

foreach my $suffix (keys(%md5)) {
    my $content;

    foreach my $hash (keys %{ $md5{$suffix} }) {
	next if scalar(keys %{ $md5{$suffix}{$hash} }) < 2;
	my $filesize = format_bytes($size{$hash})."o";
	
	print FULL "$hash,$filesize";
	$content .= "$hash,$filesize";

	my $count;
	foreach my $file (keys %{ $md5{$suffix}{$hash} }) {
	    my $dirname = dirname($file);
	    my $filename = basename($file);

	    print FULL "," if $count;
	    $content .= "," if $count;
	    $count++;
	    
	    print FULL ",$filename,$dirname\n";
	    $content .= ",$filename,$dirname\n";
	}	
    }
    
    next unless $content;
    open(SUFFIX, "> $csv_prefix.$unique$suffix.csv");  
    print SUFFIX "$title\n";
    print SUFFIX $content;
    close(SUFFIX);
}
close(FULL);

# EOF
